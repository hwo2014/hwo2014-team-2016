package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

public class Main {
    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        @SuppressWarnings("resource")
		final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, botName, botKey);
    }

    final Gson gson = new Gson();
    private PrintWriter writer;
    public Race race;
    public Boolean carOnTrack;
    public Double currentSpeed = 0.0;
    public Double prevSpeed=0.0;
    public Double prevPosition = 0.0;
    public Double inPieceRemaining;
    public Piece prevPiece, piece, nextPiece, next2Piece;
    public Integer leftLaneInd, rightLaneInd;
    
    public Main(final BufferedReader reader, final PrintWriter writer, final String botName, final String botKey) throws IOException {
        this.writer = writer;
        String line = null;

        //send(join);
		send(new Join(botName,botKey));
		/*send(new JoinRace(new BotId(botName, 
							botKey), 
							"usa", 
							"1", 
							2));
		*/
        while((line = reader.readLine()) != null) {
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            if (msgFromServer.msgType.equals("carPositions")) {
				CarPositionsMsg carPositionsMsg = gson.fromJson(line, CarPositionsMsg.class);
				List<CarPosition> carPositions = carPositionsMsg.data;
				CarPosition myCar = carByName(botName, carPositions);
				if (carOnTrack) System.out.println(myCar.toString());
                
				//Throttle
				prevPiece = piece;
				piece = nextPiece;
				if (carOnTrack) System.out.println(piece);
				nextPiece = race.track.pieces.get((myCar.piecePosition.pieceIndex+1) % race.track.pieces.size());
				next2Piece = race.track.pieces.get((myCar.piecePosition.pieceIndex+2) % race.track.pieces.size());
				//Switch
				if(!next2Piece.isStraight() && nextPiece.isStraight() & nextPiece._switch!=null) {
					if ( next2Piece.angle>0 & myCar.piecePosition.lane.toLane()!=leftLaneInd){
						send(new SwitchLane("Left"));
					}
					else if (next2Piece.angle<0 & myCar.piecePosition.lane.toLane()!=rightLaneInd){
						send(new SwitchLane("Right"));
					}
				}
				
				if (piece.isStraight() ){
					prevSpeed = currentSpeed;
					
					if (prevPiece.isStraight()) {
						currentSpeed = myCar.piecePosition.inPieceDistance-prevPosition;
					}
					prevPosition = myCar.piecePosition.inPieceDistance;
					if(carOnTrack)System.out.println("Speed: " + currentSpeed + ", acc: "+ (currentSpeed-prevSpeed));
					if(nextPiece.isStraight()) {
						if(Math.abs(myCar.angle)>45){
							send (new Throttle(0.4-0.3*(Math.abs(myCar.angle)-45)/10));
						}
						else if (next2Piece.isStraight()){
							send(new Throttle(1));
						}
						else {
							send(new Throttle(0.4));
						}
					}
					else if(piece.length - myCar.piecePosition.inPieceDistance > 70){
						send(new Throttle(0.4));
					}
					else {
						send(new Throttle(0.2));
					}
				}
				else { //Piece is bend
					//piece.radius
					Double thr = 0.3;
					if (Math.abs(myCar.angle) < 30) {
						thr = thr + 0.25 * (Math.abs(myCar.angle)/30.0);
					}
					else {
						thr = thr + 0.1 * (Math.abs(myCar.angle)-30)/30;
					}
					send(new Throttle(thr));
				}
				
            } else {
            	if (msgFromServer.msgType.equals("join")) {
                	System.out.println("Joined");
            	} else if (msgFromServer.msgType.equals("gameInit")) {
            		System.out.println("Race init");
            		carOnTrack = true;
            		RaceWrapper raceWr = gson.fromJson(line, RaceWrapper.class);
            		race = raceWr.data.race;
            		System.out.println(msgFromServer.data.toString());
            		System.out.println(raceWr.data.race.toString());
            		inPieceRemaining = race.track.pieces.get(0).length;
            		piece = race.track.pieces.get(0);
            		nextPiece = race.track.pieces.get(1);
            		next2Piece = race.track.pieces.get(2);
            		prevPiece = race.track.pieces.get(race.track.pieces.size()-1);
            		Lane tempLane1 = race.track.lanes.get(0);
            		Lane tempLane2 = race.track.lanes.get(race.track.lanes.size()-1);
            		rightLaneInd = tempLane1.distanceFromCenter > 0 ? tempLane1.index:tempLane2.index;
            		leftLaneInd = tempLane1.distanceFromCenter < 0 ? tempLane1.index:tempLane2.index;
            	} else if (msgFromServer.msgType.equals("gameEnd")) {
            		System.out.println("Race end");
            	} else if (msgFromServer.msgType.equals("gameStart")) {
            		System.out.println("Race start");
            	} else if (msgFromServer.msgType.equals("crash")) {
            		System.out.println("Car is out of track");
            		carOnTrack = false;
            	} else if (msgFromServer.msgType.equals("spawn")) {
            		System.out.println("Car is back on track");
            		carOnTrack = true;
            	} 
            	send(new Ping());
            	System.out.println("Ping sent");
            }
        }
    }

    private CarPosition carByName(String botName, List<CarPosition> carPositions) {
		if (carPositions == null) return null;
    	for(CarPosition c : carPositions) {
			if(c.id.name.equals(botName))
			{
				return c;
			}
		}
		return null;
	}

	private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }
}

abstract class SendMsg {
    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}

class MsgWrapper {
    public final String msgType;
    public final Object data;

    MsgWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData());
    }
}

class Join extends SendMsg {
    public final String name;
    public final String key;

    Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }
}


class Ping extends SendMsg {
    @Override
    protected String msgType() {
        return "ping";
    }
}

class Throttle extends SendMsg {
    private double value;

    public Throttle(double value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
}

//other important classes, are not in start package

class SwitchLane extends SendMsg {
    private String value;

    public SwitchLane(String value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "switchLane";
    }
}



class CreateRace extends SendMsg {
    public final BotId botId;
	public final String trackName;
	public final String password;
	public final Integer carCount;

    CreateRace(final BotId botId, final String trackName, final String password, final Integer carCount) {
        this.botId = botId;
		this.trackName = trackName;
		this.password = password;
		this.carCount = carCount;
    }

    @Override
    protected String msgType() {
        return "createRace";
    }
}



class JoinRace extends SendMsg {
    public final BotId botId;
	public final String trackName;
	public final String password;
	public final Integer carCount;

    JoinRace(final BotId botId, final String trackName, final String password, final Integer carCount) {
        this.botId = botId;
		this.trackName = trackName;
		this.password = password;
		this.carCount = carCount;
    }

    @Override
    protected String msgType() {
        return "joinRace";
    }
}

class BotId {
	
	public final String name;
	public final String key;
	public BotId(){name="";key="";}
	public BotId(final String name, final String key) {
		this.name = name;
		this.key = key;
	}
}

//Race info

class Race {
	public Track track;
	public List<Car> cars;
	public RaceSession raceSession;
	public Race(Track track, List<Car> cars, RaceSession raceSession) {
		super();
		this.track = track;
		this.cars = cars;
		this.raceSession = raceSession;
	}
	@Override
	public String toString() {
		return "Race [track=" + track + ", cars=" + cars + ", raceSession="
				+ raceSession + "]";
	}
	
}

class GameInfo {
	public final Race race;

	public GameInfo(Race race) {
		super();
		this.race = race;
	}
	
}
class Track {
public final String id;
public final String name;
public final List<Piece> pieces;
public final List<Lane> lanes;
public final Object startingPoint;
public Track(String id, String name, List<Piece> pieces, List<Lane> lanes,
		Object startingPoint) {
	super();
	this.id = id;
	this.name = name;
	this.pieces = pieces;
	this.lanes = lanes;
	this.startingPoint = startingPoint;
}

}

class Piece {
	@SerializedName("switch")
	public final Boolean _switch;
	public final Double length;
	public final Double radius;
	public final Double angle;
	
	
	public Piece(final Double length, final Boolean _switch) {
		super();
		this.length = length;
		this._switch = _switch;
	
		radius = 0.0;
		angle = 0.0;
	}
	public Piece(final Double length) {
		super();
		this.length = length;
	
		this._switch = false;
		radius = 0.0;
		angle = 0.0;
	}
	public Piece(final Double radius, final Double angle) {
		super();
		this.radius = radius;
		this.angle = angle;
	
		this._switch = false;
		this.length = 0.0;
	}
	public Boolean isStraight() {
		if (length == null) return false;
		else return true;
	}
	@Override
	public String toString() {
		return "Piece [_switch=" + _switch + ", length=" + length + ", radius="
				+ radius + ", angle=" + angle + ", isStraight=" + isStraight()
				+ "]";
	}
	
}

class Lane {
	public final Integer distanceFromCenter;
	public final Integer index;
	public Lane(final Integer distanceFromCenter, final Integer index) {
		super();
		this.distanceFromCenter = distanceFromCenter;
		this.index = index;
	}
	
}

class Car {
	class Id {
		public final String name;
		public final String color;
		public Id(final String name, final String color) {
			this.color = color;
			this.name = name;
		}
	}
	class Dimensions {
		public final Double length;
		public final Double width;
		public final Double guideFlagPosition;
		public Dimensions(final Double length, final Double width, 
				final Double guideFlagPosition) {
			this.guideFlagPosition = guideFlagPosition;
			this.length = length;
			this.width = width;
		}
	}
	public final Id id;
	public final Dimensions dimensions;
	public Car(final Id id, final Dimensions dimensions) {
		super();
		this.id = id;
		this.dimensions = dimensions;
	}
	
}

class RaceSession {
	public final Integer laps;
	public final Integer maxLapTimeMs;
	public final Boolean quickRace;
	public RaceSession(final Integer laps, final Integer maxLapTimeMs, final Boolean quickRace) {
		super();
		this.laps = laps;
		this.maxLapTimeMs = maxLapTimeMs;
		this.quickRace = quickRace;
	}
	
}

class RaceWrapper {
	public final String msgType;
	public final GameInfo data;
	public RaceWrapper(final String msgType, final GameInfo data) {
		super();
		this.msgType = msgType;
		this.data= data;
	}
	@Override
	public String toString() {
		return "RaceWrapper [msgType=" + msgType + ", race=" + data + "]";
	}
	
}


//carPositions
class CarPositionsMsg {
	public String msgType;
	public List<CarPosition> data;
	public CarPositionsMsg(String msgType, List<CarPosition> data) {
		super();
		this.msgType = msgType;
		this.data = data;
	}
	@Override
	public String toString() {
		return "CarPositionsMsg [msgType=" + msgType + ", carPositions="
				+ data+ "]";
	}
	
}

class CarPositionWrapper {
	public List<CarPosition> carPositions;

	public CarPositionWrapper(List<CarPosition> carPositions) {
		super();
		this.carPositions = carPositions;
	}

	@Override
	public String toString() {
		String str = "";
		for(CarPosition c: carPositions) {
			str = str + " " + c.toString();
		}
		return "CarPositionWrapper [carPositions=" + carPositions + "]";
	}
	
}
class CarPosition {
	
	class Lane {
		public Integer startLaneIndex;
		public Integer endLaneIndex;
		public Lane(Integer startLaneIndex, Integer endLaneIndex) {
			this.startLaneIndex = startLaneIndex;
			this.endLaneIndex = endLaneIndex;
		}
		public Integer fromLane() {
			return startLaneIndex;
		}
		public Integer toLane() {
			return endLaneIndex;
		}
	}
	
	public Id id;
	public Double angle;
	public PiecePosition piecePosition;
	
	public CarPosition(Id id, Double angle, PiecePosition piecePosition) {
		super();
		this.id = id;
		this.angle = angle;
		this.piecePosition = piecePosition;
		
	}
	public CarPosition() {}
	@Override
	public String toString() {
		return "CarPosition [id=" + id + ", angle=" + angle
				+ ", piecePosition=" + piecePosition + "]";
	}
	
}

class PiecePosition {
	public Integer pieceIndex;
	public Double inPieceDistance;
	public CarPosition.Lane lane;
	public Integer lap;
	public PiecePosition(Integer pieceIndex,Double inPieceDistance, 
			CarPosition.Lane lane, Integer lap) {
		this.pieceIndex = pieceIndex;
		this.inPieceDistance = inPieceDistance;
		this.lane = lane;
		this.lap = lap;
	}
	@Override
	public String toString() {
		return "PiecePosition [pieceIndex=" + pieceIndex + ", inPieceDistance="
				+ inPieceDistance + ", lane=" + lane.fromLane() + 
				" to " + lane.toLane() + ", lap=" + lap + "]";
	}
	
}

class Id {
	public String name;
	public String color;
	public Id (String name, String color) {
		this.color = color;
		this.name = name;
	}
	@Override
	public String toString() {
		return "Id [name=" + name + ", color=" + color + "]";
	}
	
}